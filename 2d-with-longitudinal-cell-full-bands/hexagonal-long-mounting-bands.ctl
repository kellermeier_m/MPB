(set! num-bands 8) ; TODO: mehr Bänder!!

(define-param RADIUS 0.24)
(define-param EPSILON (* 1.95 1.95))
(define-param MOUNTING_THICKNESS_REL 0.1 ) ; longitudinal thickness relative to longitudinal spatial period

(define lattice-constant 1 )  ; reference scale, a
(define longitudinal-constant (* 5 lattice-constant) ) ; spatial period of mounting plates in longitudinal direction
; for example if the lattice constant a in transverse direction is 200 µm, then the longitudinal lattice constant here would be 2 mm
(define longitudinal-thickness (* MOUNTING_THICKNESS_REL longitudinal-constant)) ; thickness of the mounting plates

(define Glass (make dielectric (epsilon EPSILON)))

;(set! geometry-lattice (make lattice (size lattice-constant lattice-constant lattice-constant)
          ; The third component of the lattice size is actually not relevant for
          ; defining the crystal since it's continuous in this dir. The size is
          ; only used for reference of the wavevector size
(set! geometry-lattice (make lattice (size lattice-constant lattice-constant longitudinal-constant)
                         (basis1 (/ (sqrt 3) 2) 0.5 0)
                         (basis2 (/ (sqrt 3) 2) -0.5 0)
                         (basis3 0 0 1 )
                         )
 )

(set! geometry (list (make cylinder
                       (center 0 0 0)
                       (radius RADIUS)
                       (height infinity)
                       (material Glass)
                       )
                      (make block
                        (center 0 0 lattice-constant)
                        (size infinity infinity longitudinal-thickness)
                        (material Glass)
                      )
                ))
; (define k_z (/ K_Z lattice-constant)) ; used have the input K_Z  in units of 1/a

; TODO: symmetry points in lattice
 ; https://www.researchgate.net/figure/254895063_fig2_Fig-2-The-Brillouin-zone-of-a-Primitive-hexagonal-lattice-after-19-with
(define Gamma (vector3 0 0 0))          ; Gamma
(define M (vector3 0.5 0  0 ))        ; M
(define K (vector3 (/ 3) (/ 3) 0) )     ; K
(define L (vector3 0.5 0 0.5))
(define A (vector3 0 0 0.5) )
(define H (vector3 (/ 3) (/ 3) (/ 2) ) )
(set! k-points (list 
    Gamma M K L Gamma A M H K Gamma H
))
(set! k-points (interpolate 9 k-points))

(set! resolution 32)

(run display-zparities display-yparities)
;(run-tm (output-at-kpoint (vector3 (/ -3) (/ 3) 0)
;                          fix-efield-phase output-efield-z))
;
;(run-te)
