(define-param supercellsize 5 )
(set! num-bands (* 4 supercellsize supercellsize) ) ; many more needed due to back folding to the first Brillouin zone
                    ; corresponds to band 1, 2 and 3 (3* 5^2 = 50) , since one tm gap appears

; (set! num-bands 30)                    
(set-param! tolerance 1e-7) ; defaults
; TODO---------------------------------------------                    
                    
(define-param RADIUS 0.35)
(define-param EPSILON 2.13) ; Glass at optical Freq.
(define-param K_Z_MIN 0.0)  ; Out-of-plane component
(define-param K_Z_MAX 2.0)  ; Out-of-plane component
(define-param K_Z_NUM 50)
(define-param DEFECTRADIUS 0.52) ;

(define lattice-constant 1)  ; reference scale, a

(define Glass (make dielectric (epsilon EPSILON)))
(set! default-material Glass)
;(set! geometry-lattice (make lattice (size lattice-constant lattice-constant lattice-constant)
          ; The third component of the lattice size is actually not relevant for
          ; defining the crystal since it's continuous in this dir. The size is
          ; only used for reference of the wavevector size
(define sx (* supercellsize lattice-constant))
(set! geometry-lattice (make lattice (size sx sx no-size)
                         (basis1 (/ (sqrt 3) 2) 0.5)
                         (basis2 (/ (sqrt 3) 2) -0.5)))

(set! geometry (list (make cylinder
                       (center 0 0 0)
                       (radius RADIUS)
                       (height infinity)
                       (material air)
                       )))
                       
(set! geometry (geometric-objects-lattice-duplicates geometry))
;remove rod
(set! geometry (append geometry
  (list
    (make cylinder
      (center 0 0 0)
      (radius DEFECTRADIUS)
      (height infinity)
      (material air)
    )
  )
))

(define-param k-t 0.0)

(set-param! resolution 16)

(define wvgCenter 
    (make cylinder 
      (center 0 0 0)
      (radius DEFECTRADIUS)
      (height infinity)
      (material air)
      )
)
                  
(define (defect-energy-at-band which-band)
  (get-dfield which-band)
  (compute-field-energy)
  (print
   "energy in cylinder at band " which-band " : "
   (compute-energy-in-objects wvgCenter)
   "\n")
)

(define (output-ez-without-bloch which-band) 
  (get-efield which-band)
  (cvector-field-nonbloch! cur-field)
  (output-field-z)
)

(define (run-different-kz current_kz kmax dkz)
  (if (<= current_kz kmax)
    (begin
      (set! target-freq current_kz)
      (set! k-points (list (vector3 k-t k-t current_kz)) )
      (run display-zparities display-yparities
        (output-dpwr-in-objects 
          ; (output-at-kpoint (vector3 k-t k-t 0.8) fix-efield-phase output-efield-z) 
          (combine-band-functions fix-efield-phase output-ez-without-bloch output-efield-z output-poynting-z  )
          0.1
          wvgCenter
        ) 
      )
      (run-different-kz (+ current_kz dkz) kmax dkz)
    )
  )
)

(run-different-kz K_Z_MIN K_Z_MAX (/ (- K_Z_MAX K_Z_MIN) K_Z_NUM )) 

