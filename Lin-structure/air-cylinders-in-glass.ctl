; Max Kellermeier
; 13:58 21-Aug-17
; Reproducing the result of "Photonic band gap fiber accelerator", Lin, 2001
; radius of air holes: 0.35 a
; 

(set-param! num-bands 8)
(set-param! target-freq 0) ; defaults
(set-param! tolerance 1e-7) ; defaults
; (set! mesh-size 7)

(define-param RADIUS 0.2)
(define-param EPSILON 2.13) ; Glass at optical Freq.
(define-param K_Z_MIN 0.0)  ; Out-of-plane component
(define-param K_Z_MAX 1.0)  ; Out-of-plane component
(define-param K_Z_NUM 50)

(define lattice-constant 1 )  ; reference scale, a

(define Glass (make dielectric (epsilon EPSILON)))

;(set! geometry-lattice (make lattice (size lattice-constant lattice-constant lattice-constant)
          ; The third component of the lattice size is actually not relevant for
          ; defining the crystal since it's continuous in this dir. The size is
          ; only used for reference of the wavevector size
(set! default-material Glass)
(set! geometry-lattice (make lattice (size lattice-constant lattice-constant no-size)
                         (basis1 (/ (sqrt 3) 2) 0.5)
                         (basis2 (/ (sqrt 3) 2) -0.5)))

(set! geometry (list (make cylinder
                       (center 0 0 0)
                       (radius RADIUS)
                       (height infinity)
                       (material air)
                       )))

(set-param! resolution 64)


(define (run-different-kz current_kz kmax dkz)
  (if (<= current_kz kmax)
    (begin
      ; TODO-----------------------------------
      ; (set! target-freq current_kz)
      (set! k-points (list (vector3 0 0 current_kz)          ; Gamma
                           (vector3 0 0.5 current_kz )        ; M
                           (vector3 (/ -3) (/ 3) current_kz) ; K
                           (vector3 0 0 current_kz)))        ; Gamma
      (set! k-points (interpolate 9 k-points))
      (run display-zparities display-yparities)

      (run-different-kz (+ current_kz dkz) kmax dkz)
    )
  )
)
(run-different-kz K_Z_MIN K_Z_MAX (/ (- K_Z_MAX K_Z_MIN) K_Z_NUM )) 

;(run-tm (output-at-kpoint (vector3 (/ -3) (/ 3) 0)
;                          fix-efield-phase output-efield-z))
;
;(run-te)
