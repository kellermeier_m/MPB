The structure defined in the folders '1-cell' and '2-cells' are defined with an infinite mounting. 
The number of rods removed in the center is 7, as depicted in epsilon.png in '1-cell'. In both cases the remaining lattice cells in the transverse plane are 3. 
- In transverse direction no translational symmetry is assumes anymore due to being a finite "crystal". 
- Here, only the periodic mounting along the waveguide axis is treated as a infinite lattice of which the band structure is studied.

TODO
===
- finite mounting, compared to infinitely extended mounting in transverse direction
  (done now in folder "1-cell-finite-mounting"; has still 7 rods removed)
- more bands to go to higher frequencies
- single rod removed instead of 7 rods in the center removed 
   (currently running)
