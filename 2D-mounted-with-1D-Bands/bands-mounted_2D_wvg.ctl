(set! num-bands 15 )

;(define-param DPML 1.) ; Used as thickness of the Perfectly matched layer
;(define-param PAD 4.) ; padding from wall

(define-param RADIUS 0.2)
(define-param EPSILON (* 1.95 1.95))
(define-param MOUNTING_THICKNESS_REL 0.1 ) ; longitudinal thickness relative to longitudinal spatial period 'longitudinal-constant'
(define-param K_Z 0.0)  ; Out-of-plane component of the

(define lattice-constant 1 )  ; reference scale, a
(define longitudinal-constant (* 5 lattice-constant) ) ; longitudinal spatial period c, in terms of a
(define Glass (make dielectric (epsilon EPSILON)))
(define basis_1 (vector3* lattice-constant (vector3 1 0 0) ))
(define basis_2 (vector3* lattice-constant ( vector3 0.5 (/ (sqrt 3) 2) 0)) )
(define basis_3 (vector3 0 0 longitudinal-constant ))
;
;

(define-param NUM_CELL_1 5)
(define-param NUM_CELL_2 NUM_CELL_1 )
(define-param NUM_CELL_LONGITUDINAL 0) ; number of repitions of cell. A single cell is always used

(define-param NUM_CELLS_WVG 1) ; number of cells of rods which will be removed in the center for the waveguide; 0 means the single center rod is removed


(define-param MOUNTING_RADIUS (* (+ NUM_CELL_1 1) lattice-constant ) ) ; radius of mounting plate is one cell larger than the rod lattice to make sure all rods are completely covered
;
; (define-param GRID_SIZE_X 10) ; useful size needed
(define-param GRID_SIZE_X
    (* 2 (+
        (* NUM_CELL_1 (vector3-x basis_1) )
        2
        )
    )
)
(define-param GRID_SIZE_Y
    (* 2 (+
        (* NUM_CELL_2 (vector3-y basis_2) )
        2
        )
    )
)
;; Put mounting at the beginning and at the end of the rods
(define-param GRID_SIZE_Z
    ( * (+ 1 NUM_CELL_LONGITUDINAL) longitudinal-constant)
)
; (NUM_CELL_LONGITUDINAL * longitudinal-constant )

; ----------------------------------------------------------------------------
(set! geometry-lattice (make lattice
    (size GRID_SIZE_X GRID_SIZE_Y GRID_SIZE_Z)
    ;(size 10 10 10)
    (basis3 basis_3)
))

;; surrounding material: air
;;(set! default-material air )

(define geometryList
  (geometric-object-duplicates
  basis_1 ; shift-vector
  (* -1 NUM_CELL_1) ; min-multiple
  NUM_CELL_1 ; max-multiple
  (make cylinder (center 0 0 0) (radius RADIUS) (height infinity) (material Glass) )
))

; remove part from parallelogram to make it hexagonal
; center
(define thicknessAirBlock
    (* (- (vector3-norm (vector3+ basis_1 basis_2)) RADIUS)
        NUM_CELL_1
    )
)
(define corner (vector3+
  (vector3* NUM_CELL_1 basis_1)
  (vector3* NUM_CELL_2 basis_2)
))

(define cAirBlock
  (vector3*
    corner
  (+ 0.75 (/
    (* 2.1 RADIUS)   ; a little bit more than a diameter
    (vector3-norm corner) ) )
  )
)

(set! geometryList (append
    ; Construct parallelogram of lattice cells
    (geometric-objects-duplicates
        basis_2 ; shift-vector
        (* -1 NUM_CELL_2) ; min-multiple
        NUM_CELL_2 ; max-multiple
        geometryList
    )
    ; cut off parallelogram at upper right corner
    (list
        (make block
            (center cAirBlock )
            (size (* 0.5 thicknessAirBlock) thicknessAirBlock infinity)
            (e1 1.5 (/ (sqrt 3) 2) ) ; sum of basis_1 and basis_2
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )
    ;    ; cut off parallelogram at lower bottom corner
        (make block
            (center (vector3* -1 cAirBlock ))
            (size (* 0.5 thicknessAirBlock) thicknessAirBlock infinity)
            (e1 1.5 (/ (sqrt 3) 2) )
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )

    )
  )
)


(define mountingCenter (vector3* 0.5 basis_3))
(if  (= (remainder NUM_CELL_LONGITUDINAL 2 ) 0 )
  (define mountingCenter (vector3 0 0 0))
  (define mountingCenter (vector3* 0.5 basis_3))
  
)
; Periodic mounting
; TODO: Needs improvement for supercell lattice
(set! geometryList ( append geometryList
    (geometric-object-duplicates
      basis_3 ; direction for the duplicates
      (* -1 NUM_CELL_LONGITUDINAL)  ; minimum of the multiples of the direction
      0 ; maximum of the multiples of the direction
      (make cylinder  ; geometric object
        (center mountingCenter )
        (radius MOUNTING_RADIUS)
        ; (radius infinity)
        (height (* MOUNTING_THICKNESS_REL longitudinal-constant ) )
        (material Glass)
      )
    )
  )
)

(set! geometryList ( append geometryList
; Waveguide hole
  (list
    (make cylinder
      (center 0 0 )
      (radius (+ RADIUS (* (vector3-x basis_1) NUM_CELLS_WVG) ) )
      (height infinity)
      (material air)
    )
  )
  )
)
(set! geometry geometryList)
;(set! resolution 16)
(print "transformed reciprocal lattice vector k=(0,0,0.5): "
  (reciprocal->cartesian (vector3 0 0 0.5)) "\n" )

(set! resolution 8)

(define k-points-list (list
      (vector3 0 0 0)
      (vector3 0 0 0.5)
 ))

(define-param k-interp  39) ; usually 39
(set! k-points  (interpolate k-interp k-points-list))

(run 
  (output-at-kpoint (vector3 0 0 0.5)
                          fix-efield-phase output-efield-z)
  (output-at-kpoint (vector3 0 0 0)
                          fix-efield-phase output-efield-z)
)


;; (if only-eps?
;  ; (run-until 1
;    ; ; (at-beginning output-epsilon)
;  ; )
;  ; ; else
;  ; (begin
;    ; (run-sources+ 300
;      ; ;(at-beginning output-epsilon)
;      ; (after-sources (harminv Ez srcCenter fcen df))
;      ; )
;    ; (if excite-mode?
;      ; (run-until (/ 1 fcen)
;        ; (to-appended "ez" (at-every (/ 1 fcen 20) output-efield-z) )
;        ; ; (at-every  output-efield-z)
;      ; )
;    ; )
;  ; )
;; )
;
;; (run-until 1 ( at-beginning output-epsilon))
