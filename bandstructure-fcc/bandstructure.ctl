; Based on tri-rods.ctl in the Tutorial
; Geometry parameters
; Distance between rods in x resp. y direction
(define-param DISTANCE 1.)  ; corresponds to 'a'  in Cowan, B.
(define-param VERTICAL_PERIOD (* DISTANCE (sqrt 2)) ); corresponds to 'c'  in Cowan, B.
; (set! VERTICAL_PERIOD DISTANCE )

; Rod dimensions
(define-param RODWIDTH 0.25)  ; corresponds to 'w'  in Cowan, B.
(define-param RODHEIGHT (* VERTICAL_PERIOD 0.25 ) ) ; corresponds to 'c'/4  in Cowan, B.,
      ; c= a * sqrt(2) gives a cubic lattice
; Material
; (define-param EPSILON 12.1) ; Silicon
(define-param EPSILON (* 1.95 1.95))  ; Glass

; (define-param GRID_SIZE_X DISTANCE) ;
; (define-param GRID_SIZE_Y DISTANCE) ;
; (define-param GRID_SIZE_Z (+ (* 4 RODHEIGHT)  6 )) ; increased size due to added pml layer
; (define-param DPML PAD)  ; thickness of the pml layer

(define Silicon (make dielectric (epsilon 12.1)))
(define Glass (make dielectric (epsilon EPSILON)))

; ----------------------------------------------------------------------------

(set! num-bands 2)
; (set! geometry-lattice (make lattice (size 1 1 no-size)
                         ; (basis1 (/ (sqrt 3) 2) 0.5)
                         ; (basis2 (/ (sqrt 3) 2) -0.5)))

(define thirdBasisVec (vector3 
                    (/ DISTANCE 2)
                    (/ DISTANCE 2)
                    (/ VERTICAL_PERIOD 2)
))

; Norm of the vector defined in the previous line
(define thirdBasisVec_len (* 0.5 
    (sqrt (+ (* DISTANCE DISTANCE) 
            (* DISTANCE DISTANCE)
            (* VERTICAL_PERIOD VERTICAL_PERIOD)
         ) )
    )
)
                         
(set! geometry-lattice (make lattice (size 1 1 1)
                            (basis1 1 0 0)  
                            (basis2 0 1 0)
                            (basis3 thirdBasisVec)
                            (basis-size DISTANCE DISTANCE thirdBasisVec_len)
                        )
)                       
 
; Probably try http://ab-initio.mit.edu/wiki/index.php/MPB_User_Reference#Coordinate_conversion_functions
; instead of manually writing the vector 
(define geometryDir (vector3 
                    (/ -1 VERTICAL_PERIOD )
                    (/ -1 VERTICAL_PERIOD )
                    (/ 2 VERTICAL_PERIOD )
))
 
(define _rodHeight_shifted (* VERTICAL_PERIOD 0.5 RODHEIGHT) ) 
(set! _rodHeight_shifted RODHEIGHT)

(set! geometry (list (make block
                       (center 0 0 -0.25) 
                       (size RODWIDTH DISTANCE _rodHeight_shifted) 
                       (e1 1 0 0)
                       (e2 0 1 0)
                       (e3 geometryDir ) ; probably different length
                       ; (e3 0 0 1)
                       (material Glass )
                       )
                     (make block
                       (center 0 0 0.25) 
                       (size DISTANCE RODWIDTH _rodHeight_shifted) 
                       (e1 1 0 0)
                       (e2 0 1 0)
                       ; (e3 0 0 1)
                       (e3 geometryDir )
                       (material Glass)
                       )
                ))

                       
(set! k-points (list (vector3 0 0 0)          ; Gamma
                     (vector3 0 0.5 0)        ; M
                     (vector3 (/ -3) (/ 3) 0) ; K
                     (vector3 0 0 0)))        ; Gamma
(set! k-points (interpolate 2 k-points))

(set! resolution 64)

; (use-output-directory)
(run-tm (output-at-kpoint (vector3 (/ -3) (/ 3) 0) 
			  fix-efield-phase output-efield-z))
(run-te)




