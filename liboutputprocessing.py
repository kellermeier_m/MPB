import numpy as np
from matplotlib import pyplot as plt
import matplotlib.ticker as plticker
from mpl_toolkits.mplot3d import Axes3D
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets

# %matplotlib notebook


# correct data since the 'zparity' rows have no k1 k2 k3 kmag components and therefore 4 fields less than the freq rows
def check_csv(fname):
    '''
    This function is used to correct datasets having mismatching rows accoring to their fields. This can therefore not 
    be read as an array. E.g. a file like
        band1, band2, band3, band4
        0.13,  0.185, 0.851,  0.19
        0.28,   0.13,  0.87
        0.961, 0.149,  0.58,  0.28
    has a missing field in the second row at the end. This function changes the second row to
        0.28,   0.13,  0.87, 
    so it adds a comma such that the row contains an empty field at the end. Keep in mind that a function reading in the 
    data has to take care of the missing data. As an example missing data could be set to NaN.
    
    THIS FUNCTION OVERRIDES THE INPUT FILE
    '''
    file_lines = ""
    with open(fname, 'r') as f:
        file_lines = [x for x in f.readlines()]
        max_commas = max([x.count(",") for x in file_lines])
#         for line in f:
#             count = line.count(',')
#             max_commas = max_commas if (max_commas > count) else count
#         print(max_commas)
    
    with open(fname, 'w') as f:
        for line in file_lines:
            f.write(line[:-1] + "," * (max_commas - line.count(',')) + "\n")
            
def correct_shift_bet_freq_zparity(fname):
    '''
    While the 'freqs:' rows have the format
        k_index, k_1, k_2, k_3, k_mag, band 1, band 2,...
    the 'zparity:' rows have only
        k_index, band 1, band 2,...
    Therefore each zparity row is extended by four empty cells at the beginning.
    
    THIS FUNCTION OVERRIDES THE INPUT FILE
    '''
    lines = []
    with open(fname, 'r') as f:        
        lines = f.readlines()
    
    with open(fname, 'w') as f:        
        for line in lines:
            if line.startswith("zparity:"):
                f.write(line.replace("zparity:", "zparity:,,,,"))
            else:
                f.write(line)            