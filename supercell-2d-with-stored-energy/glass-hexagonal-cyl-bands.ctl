(define-param supercellsize 5 )
(set! num-bands (* 3 supercellsize supercellsize) ) ; many more needed due to back folding to the first Brillouin zone
                    ; corresponds to band 1, 2 and 3 (3* 5^2 = 50) , since one tm gap appears

(define-param RADIUS 0.245) ; changed radius
(define-param EPSILON (* 1.95 1.95))
(define-param K_Z 0.0)  ; Out-of-plane component
(define-param DEFECTRADIUS RADIUS) ;

(define lattice-constant 1)  ; reference scale, a


(define Glass (make dielectric (epsilon EPSILON)))

;(set! geometry-lattice (make lattice (size lattice-constant lattice-constant lattice-constant)
          ; The third component of the lattice size is actually not relevant for
          ; defining the crystal since it's continuous in this dir. The size is
          ; only used for reference of the wavevector size
(define sx (* supercellsize lattice-constant))
(set! geometry-lattice (make lattice (size sx sx no-size)
                         (basis1 (/ (sqrt 3) 2) 0.5)
                         (basis2 (/ (sqrt 3) 2) -0.5)))

(set! geometry (list (make cylinder
                       (center 0 0 0)
                       (radius RADIUS)
                       (height infinity)
                       (material Glass)
                       )))
(set! geometry (geometric-objects-lattice-duplicates geometry))
;remove rod
(set! geometry (append geometry
  (list
    (make cylinder
      (center 0 0 0)
      (radius DEFECTRADIUS)
      (height infinity)
      (material air)
    )
  )
))


(define k_z (/ K_Z lattice-constant)) ; used have the input K_Z  in units of 1/a
;
;(set! k-points (list (vector3 0 0 k_z)          ; Gamma
;                     (vector3 0 0.5 k_z )        ; M
;                     (vector3 (/ -3) (/ 3) k_z) ; K
;                     (vector3 0 0 k_z)))        ; Gamma
;(set! k-points (interpolate 9 k-points))

; range of tm band gap 0.44 0.53
(set! k-points (list (vector3 0.5 0.5 k_z)))
; (set! k-points (list (vector3 0.0 0.0 k_z)))
(set! resolution 16)

(define (defect-energy-at-band which-band)
  (get-dfield which-band)
  (compute-field-energy)
  (print
   "energy in cylinder at band " which-band " : "
   (compute-energy-in-objects (make cylinder (center 0 0 0)
                                    (radius 1.0)
                                    (height infinity)
                                    (material air)))
   "\n")
  ; ...do stuff here with band index which-band...
)


(run 
  defect-energy-at-band display-zparities display-yparities)

(define outputFields? False)
(if outputFields?
(for-each (output-at-kpoint (vector3 0.5 0.5 k_z)
    fix-efield-phase output-efield-z )
  '(85 86 87 88 89 90 91 92 93 94 95 96)
)    
)


;TODO: does this function work?


;(output-efield-z 45)
;(get-dfield 45)
;(compute-field-energy)
;(print
; "energy in cylinder at band 45: "
; (compute-energy-in-objects (make cylinder (center 0 0 0)
;                                  (radius 1.0) (height infinity)
;                                  (material air)))
; "\n")



;(run-tm (output-at-kpoint (vector3 (/ -3) (/ 3) 0)
                          ;fix-efield-phase output-efield-z))
;
;(run-te)
