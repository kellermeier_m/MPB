(define-param supercellsize 5 )
(set-param! num-bands (* 3 supercellsize supercellsize) ) ; many more needed due to back folding to the first Brillouin zone
                    ; corresponds to band 1, 2 and 3 (3* 5^2 = 50) , since one tm gap appears

(define-param RADIUS 0.245) ; changed radius
(define-param EPSILON (* 1.95 1.95))
(define-param DEFECTRADIUS 1.) ;

(define lattice-constant 1)  ; reference scale, a
(define Glass (make dielectric (epsilon EPSILON)))

(define sx (* supercellsize lattice-constant))
(set! geometry-lattice (make lattice (size sx sx no-size)
                         (basis1 (/ (sqrt 3) 2) 0.5)
                         (basis2 (/ (sqrt 3) 2) -0.5)))

(set! geometry (list (make cylinder
                       (center (/ 3) (/ 3) 0)
                       (radius RADIUS)
                       (height infinity)
                       (material Glass)
                       )))
(set! geometry (geometric-objects-lattice-duplicates geometry))
;remove rod
(set! geometry (append geometry
  (list
    (make cylinder
      (center 0 0 0)
      (radius DEFECTRADIUS)
      (height infinity)
      (material air)
    )
  )
))

(define-param num-k 9)
(define-param k-z-min 0.0)
(define-param k-z-max 1.0)
(define-param k-t 0.0)
(set! k-points 
  (interpolate num-k (list (vector3 k-t k-t k-z-min) (vector3 k-t k-t k-z-max)))
)


(set-param! resolution 64)

(define wvgCenter 
    (make cylinder 
      (center 0 0 0)
      (radius DEFECTRADIUS)
      (height infinity)
      (material air)
      )
)    

(define (run-different-kz current_kz kmax dkz)
  (if (<= current_kz kmax)
    (begin
      (set! target-freq current_kz)
      (set! k-points (list (vector3 k-t k-t current_kz)) )
      (run display-zparities display-yparities
        (output-dpwr-in-objects 
          ; (output-at-kpoint (vector3 k-t k-t 0.8) fix-efield-phase output-efield-z) 
          (combine-band-functions fix-efield-phase output-ez-without-bloch output-efield-z output-poynting-z  )
          0.3
          wvgCenter
        ) 
      )
      (run-different-kz (+ current_kz dkz) kmax dkz)
    )
  )
)

; (run-different-kz K_Z_MIN K_Z_MAX (/ (- K_Z_MAX K_Z_MIN) K_Z_NUM )) 

(run 
;  display-zparities display-yparities defect-energy-at-band
  (output-dpwr-in-objects 
    ; (output-at-kpoint (vector3 k-t k-t 0.8) fix-efield-phase output-efield-z) 
    (combine-band-functions fix-efield-phase output-efield-z output-poynting-z  )
    0.25
     wvgCenter
  ) 
)
