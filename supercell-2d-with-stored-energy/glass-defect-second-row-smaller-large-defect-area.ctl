(define-param supercellsize 7 )
(set! num-bands (* 3 supercellsize supercellsize) ) ; many more needed due to back folding to the first Brillouin zone
                    ; corresponds to band 1, 2 and 3 (3* 5^2 = 50) , since one tm gap appears
(set! num-bands 30)

(define-param RADIUS 0.245) ; changed radius
(define-param EPSILON (* 1.95 1.95))
(define-param K_Z 0.0)  ; Out-of-plane component
(define-param DEFECTRADIUS RADIUS) ;
(define-param SMALLER_RADIUS 0.1)

(define lattice-constant 1)  ; reference scale, a


(define Glass (make dielectric (epsilon EPSILON)))

;(set! geometry-lattice (make lattice (size lattice-constant lattice-constant lattice-constant)
          ; The third component of the lattice size is actually not relevant for
          ; defining the crystal since it's continuous in this dir. The size is
          ; only used for reference of the wavevector size
(define sx (* supercellsize lattice-constant))
(set! geometry-lattice (make lattice (size sx sx no-size)
                         (basis1 (/ (sqrt 3) 2) 0.5)
                         (basis2 (/ (sqrt 3) 2) -0.5)))

(set! geometry (list (make cylinder
                       (center 0 0 0)
                       (radius RADIUS)
                       (height infinity)
                       (material Glass)
                       )))
(set! geometry (geometric-objects-lattice-duplicates geometry))
;remove rod
(set! geometry (append geometry
  (list
    (make cylinder
      (center 0 0 0)
      (radius DEFECTRADIUS)
      (height infinity)
      (material air)
    )
  )
))


;----- TODO
(define v1 (vector3  (/ (sqrt 3) 2 ) 0.5) )
(define v2 (vector3  (/ (sqrt 3) 2 ) -0.5) )
(define geometrySmallRods
  (list 
    (make cylinder
      (center 1 0 ) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
    (make cylinder
      (center 0 1) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
    (make cylinder
      (center 1 -1 ) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
    (make cylinder
      (center -1 0 ) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
    (make cylinder
      (center 0 -1 ) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
    (make cylinder
      (center -1 1 ) 
      (radius SMALLER_RADIUS)
      (height infinity)
      (material Glass)
    )
  )
)

(set! geometry (append geometry
  geometrySmallRods
))



(define k_z (/ K_Z lattice-constant)) ; used have the input K_Z  in units of 1/a
;
;(set! k-points (list (vector3 0 0 k_z)          ; Gamma
;                     (vector3 0 0.5 k_z )        ; M
;                     (vector3 (/ -3) (/ 3) k_z) ; K
;                     (vector3 0 0 k_z)))        ; Gamma
;(set! k-points (interpolate 9 k-points))

(define-param num-k 9)
(define-param k-z-min 0.0)
(define-param k-z-max 1.0)
(define-param k-t 0.0)
;(set! k-points 
;  (interpolate num-k (list (vector3 k-t k-t k-z-min) (vector3 k-t k-t k-z-max)))
; )

(set! resolution 32)

(define wvgCenter 
    (make cylinder 
      (center 0 0 0)
      (radius 1.3)
      (height infinity)
      (material air)
      )
)
                  

(define (output-ez-without-bloch which-band) 
  (get-efield which-band)
  (cvector-field-nonbloch! cur-field)
  (output-field-z)
)

(define (run-different-kz current_kz kmax dkz)
  (if (<= current_kz kmax)
    (begin
      (set! target-freq current_kz)
      (set! filename-prefix (string-append "kz." (number->string current_kz) "-" ) ) 
      (set! k-points (list (vector3 k-t k-t current_kz)) )
      (run display-zparities display-yparities
        (output-dpwr-in-objects 
          ; (output-at-kpoint (vector3 k-t k-t 0.8) fix-efield-phase output-efield-z) 
          (combine-band-functions fix-efield-phase output-ez-without-bloch output-efield-z)
          0.35
          wvgCenter
        ) 
      )
      (run-different-kz (+ current_kz dkz) kmax dkz)
    )
  )
)

(run-different-kz k-z-min k-z-max (/ (- k-z-max k-z-min) (+ num-k 1) ))

   
