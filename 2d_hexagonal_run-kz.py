# -*- coding: utf-8 -*-
"""
Created on Mon Apr 25

@author: maxke
"""
from __future__ import print_function
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.ticker as plticker
#from subprocess import call
# import time
import os
import regex as re

def run_mpb(main_dir, k_z=0., r=None, dir_path='varied_k_z'):
    '''
    This functions runs MPB for a effectively two dimensional crystal of
    hexagonally aranged cylindrical rods of glass.

    k_z is the out-of-plane vector in whose direction the translational symmetry is continuous,
        not discrete as in the plane.
    r is the radius of the cylinders
    '''
    os.chdir(main_dir)
    # os.chdir("2d-hexagonal")
    # os.chdir("supercell-2d-hexagonal")
    if r is not None:
        cmd_mpb = "mpb K_Z={0:.3f} RADIUS={1:.3f} glass-hexagonal-cyl-bands.ctl | tee {2}/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.out".format(k_z, r, dir_path)
    else:
        cmd_mpb = "mpb K_Z={0:.3f} glass-hexagonal-cyl-bands.ctl | tee {1}/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.out".format(k_z, dir_path)
    print("TO BE EXECUTED:", cmd_mpb)
    os.system(cmd_mpb)
    os.chdir("..")

    # After MPB has finished
    # grep freq glass/bands-cuboid-rod-cell-ratio-0.33.out > glass/bands-cuboid-rod-cell-ratio-0.33.freq.dat

def extract_freq(main_dir, k_z=0., r=None, dir_path='varied_k_z'):
    os.chdir(main_dir)
    cmd_grep= "grep freq {1}/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.out > {1}/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.freq.dat".format(k_z, dir_path)
    print("TO BE EXECUTED:", cmd_grep)
    os.system(cmd_grep)
    os.chdir("..")

def extract_z_parity(main_dir, k_z=0., dir_path='varied_k_z'):
    '''
    This functions runs MPB for a effectively two dimensional crystal of
    hexagonally aranged cylindrical rods of glass.

    k_z is the out-of-plane vector in whose direction the translational symmetry is continuous,
        not discrete as in the plane.
    r is the radius of the cylinders
    '''
    os.chdir(main_dir)
    # After MPB has finished
    # grep zparity glass/bands-cuboid-rod-cell-ratio-0.33.out > glass/bands-cuboid-rod-cell-ratio-0.33.freq.dat
    cmd_parity= "grep zparity:  {1}/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.out > {1}/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.zparity.dat".format(k_z, dir_path)
    print("TO BE EXECUTED:", cmd_parity)
    os.system(cmd_parity)
    os.chdir("..")

def generate_band_files(main_dir, numBands = 8, dir_path='varied_k_z'):
    '''
    For each band returned from MPB a file is created containing all frequencies
        depending on the wavevector.
    '''
    current_band = 1
    os.chdir("{0}/{1}".format(main_dir, dir_path))

    # os.chdir("supercell-2d-hexagonal/{0}".format(dir_path))
    for file in os.listdir('.'):
        if file.endswith(".freq.dat"):
            # scheme of the files:
            # freqs:, k index, kx, ky, kz, kmag/2pi, band 1, band 2, band 3, band 4, band 5, band 6, band 7, band 8
            mpb_output = np.genfromtxt(file, skip_header=1, delimiter=",", dtype="str")

            for i in range(numBands):
                with open('{0}.band.dat'.format(i+1), "ab") as f:
                # write kx, ky, kz, band i
                    # print(mpb_output[ :, [2,3,4, 6+i] ].dtype)
                    np.savetxt(f, mpb_output[ :, [2,3,4, 6+i] ], fmt='%s', delimiter=",")

            # print(os.path.join("/mydir", file))

    os.chdir('../..')

    # tm_bands_import= np.genfromtxt(path_freq_output, skip_header=1, delimiter=",", dtype="str")
    # tm_bands_data = tm_bands_import[:,1:].astype(float)

def combine_parity_freq(main_dir):
    with open(dir+"/varied_k_z.freq_zparity_energy.dat", "a") as f:
            for k_z in np.linspace(0,5, 50):
                freq_by_kz = dir+ "/varied_k_z/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.freq.dat".format(k_z)
                with open(freq_by_kz, 'r') as kz_file:
                    f.write(kz_file.read())
                parity_by_kz = dir + "/varied_k_z/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.zparity.dat".format(k_z)
                with open(parity_by_kz, 'r') as kz_file:
                    f.write(kz_file.read())


def freq_parity_energy_from_out(main_dir, dir_path="varied_k_z"):
    with open(main_dir+"/varied_k_z.freq_zparity_energy.dat", "a") as result_f:
        for k_z in np.linspace(0,5, 50):
            # extract freq
            with open("{2}/{1}/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.out".format(k_z, dir_path, main_dir), "r" ) as sim_f:
                freqs_lines= [s for s in sim_f.readlines() if re.search("freqs", s)]
                result_f.writelines(freqs_lines)
            #extract zparity
            with open("{2}/{1}/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.out".format(k_z, dir_path, main_dir), "r" ) as sim_f:
                zparity_lines= [s for s in sim_f.readlines() if re.search("zparity", s)]
                result_f.writelines(zparity_lines)
            # extract energy
            with open("{2}/{1}/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.out".format(k_z, dir_path, main_dir), "r" ) as sim_f:
                result_f.write("energy:,,,,,")   # only three commas added due to initial comma in loop
                for l in map( lambda s: str.split(s)[-1],
                             [s[:-1] for s in sim_f.readlines() if re.search("energy in", s)]
                            ):
                    result_f.write(", "+ l )
                result_f.write("\n")



if __name__ == '__main__':
    main_dir= "2d-hexagonal"
    # main_dir = "supercell-2d-hexagonal"
    # main_dir= 'supercell-2d-with-stored-energy'
    for kz in np.linspace(0,1,50, endpoint=False):
        extract_freq(main_dir, k_z= kz, dir_path= 'varied_k_z')

    # for kz in np.linspace(0,1, 50, endpoint=False):
        # run_mpb(main_dir, k_z= kz, r=0.3, dir_path= 'varied_k_z')
    # freq_parity_energy_from_out(main_dir)

        # extract_z_parity("supercell-2d-hexagonal", k_z= kz, dir_path= 'varied_k_z' )

    # generate_band_files(dir_path= 'varied_k_z')
