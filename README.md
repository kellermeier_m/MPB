# MPB Simulations
MPB - MIT Photonic Bands - is a software developed at MIT by Steven G. Johnson. It is a frequency domain solver designed for Photonic band gap structures which takes the dielectric function in a specified unit cell to compute bands of electromagnetic modes in such a geometry.

More information can be found on the (official webpage)[http://ab-initio.mit.edu/wiki/index.php/MIT_Photonic_Bands] of MPB.

The goal of the simulations done with this repo is the design of a dielectric structure for electron Accelaration based on a Photonic band gap geometry.

# Script language for control
Scheme/Guile is used for controlling the simulations. MPB and MEEP work with an extension library of guile called libctl.
The control file specifies the geometric and material properties, sets the lattice of the geometry and the resolution, takes a list of k-vectors and finally specifies the output of the runs. Properties of modes of interest can also fixed.

# Directories
* tutorial: Used for learning the language (Scheme/Guile) to control the simulations.
* simple cubic: This is a geometry of the primitive cell with the cell content from ("On the Design of Millimetre Wave EBG Resonator Antenna Based on Woodpile Structure", Lee, Hao, Zhao, Parini )[http://ieeexplore.ieee.org/abstract/document/4125957/?reload=true]   
   TODO: images   
   All simulations were controlled by "bandstructure-simple.ctl"
  - glass: Fused Silica glass with permittivity at THz freq. ( ca. 3.9 ) is used.
  - aluminum: This simulation aims on reproducing the results from the paper mentioned above
  - silicon: The original work on Laser acceleration in a PBG woodpile structure was done with silicon and a IR laser (1500 nm). For the band structure a a different unit cell was chosen originally. Here, the results with the "simple-cubic" cell show the same band gap.
* bandstructure-fcc: aims on verifying the results from the previous band structure with glass but with a different definition of the unit cell. The work is not finished.
* bandstructure-ben: aims on verifying the previous results with glass but with the unit cell from the original work on silicon woodpiles.
* 2d-hexagonal: new geometry, the problem is effectively two dimensional. cylindrical glass rods are aranged parallel w.r.t. the cylinder axis but with a hexagonal/trigonal lattice arrangement in the radial plane. This is currently under work. The band structure for the effectively 2-dimensional problem was calculated as well as with varied longitudinal wavevector
* 2d-with-longitudinal-cell: The cylinders have to be mounted in longitudinal direction. A mounting plate is used in regular spacings, defining a periodicity along the cylindrical axis. currently under work