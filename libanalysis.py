import numpy as np
from matplotlib import pyplot as plt
import matplotlib.ticker as plticker
from mpl_toolkits.mplot3d import Axes3D
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
import os
import glob
import pandas as pd

def read_bands_freq(fpath):
    bands_import= np.genfromtxt(fpath, skip_header=1, delimiter=",", dtype="str")
    return bands_import[:, 1:].astype(float)

def plot_projected_bands_from_h5(fpath):
    '''
    Parameters:
        fpath: string
            path to the hdf5 file containing the band information of the structure. It is a pandas dataframe with kz at the first index level, kt at the second index level and the band labels as columns
            
    '''
    band_data= pd.read_hdf(fpath)
    max_bands=band_data.max(axis=0, level="kz")
    min_bands=band_data.min(axis=0, level="kz")
    kz=max_bands.index
    
    #fig= plt.figure()
    fig = plt.figure(figsize=plt.figaspect(0.8))
    ax=fig.add_subplot(111)
    next_color = plt.rcParams['axes.prop_cycle'].by_key()['color'][0]
    for i, (min_band, max_band) in enumerate(zip(min_bands.T.values, max_bands.T.values)):
        polyCol = ax.fill_between(kz.data, min_band.data, max_band.data, color=next_color, zorder=10) 
    ax.plot(kz.data,kz.data,"r-", label="SOL Dispersion", zorder=11)
    ax.set_xlabel(r'$k_z a/ 2 \pi$')
    ax.set_ylabel(r'$\omega a/ 2 \pi c $')
    ax.set_title(r"Projected Band Diagram")
    ax.legend()
    return ax


def plot_projected_bands(dirpath, sameColor=True, numbands=7, num_kz=10, num_kt_interpolate=9, skipbands=None):
    '''
    TODO: Documentation
    numbands not used yet
    
    single lines without a colormatching area come from an overlayed area of different color. E.g. the blue area from band 1
    is behind the orange one from band 2

    Parameters:
        dirpath: path to the directory containing the bandstructure files in the format
                glass-hexagonal-cyl-bands-k_z-0.000.freq.dat
                The value for k_z is only an example.
        numbands: number of bands to plot
        sameColor: boolean,
                if True the band ranges are all plotted with the same color. In this case the are not distinguishable anymore. One can only see, where band gaps occur
                If False each band gets its own color.
        skipbands: integer or array,
                An array containing the numbers of bands which shouldn't be plotted. If it's only a single value, the band according to this number won't be plotted.
                The default is None, which means all bands will be plotted.
    '''
    filelist=glob.glob(os.path.join(dirpath, "glass-hexagonal-cyl-bands-k_z-*.freq.dat"))
    
    kz_iter= map(lambda filepath: float(os.path.split(filepath)[1][-14:-9]), filelist)
    kz= np.fromiter(kz_iter, dtype=float)
    # within one of the arrays:
    # k index, k1, k2, k3, kmag/2pi, band 1, band 2, band 3, band 4, band 5, band 6, band 7, band 8
    
    data= np.array( [ read_bands_freq(filepath) for filepath in filelist ] )
#     data= np.array( [ read_bands_freq("../Simulation-results/MPB/2d-hexagonal/varied_k_z_old/glass-hexagonal-cyl-bands-k_z-{0:.3f}.freq.dat".format(i))
#          for i in np.linspace(0,1,20, endpoint=False) ] )
#     data_t = data[:5,:5, 5:10]
#     print(data_t)
    min_bands = data[:,:,5:].min(axis=1)
    max_bands = data[:,:,5:].max(axis=1)
    k_z = data[:,0,3]
    
    fig = plt.figure(figsize=plt.figaspect(0.75))
    ax1= fig.add_subplot(1,1,1)
#     polyCol = ax1.fill_between(k_z, min_bands[:,0], max_bands[:,0])
#     print(polyCol.get_facecolor()[0])
    band_indices = np.arange(numbands)
    if not (skipbands is None):
        skipbands= np.array([skipbands]).reshape((-1,))
        # exclude second band (with index 1)
        #bands_indices = np.array([0,2,3,4,5,6,7])
        band_indices= filter_out(band_indices, skipbands)
        min_bands = min_bands[:, band_indices ]
        max_bands = max_bands[:, band_indices ]


    next_color = plt.rcParams['axes.prop_cycle'].by_key()['color'][0]
    for (i, min_band, max_band) in zip(band_indices, min_bands.T, max_bands.T): 
        polyCol = ax1.fill_between(k_z, min_band, max_band, color=next_color) 
        # if sameColor else ax1.fill_between(k_z, min_band, max_band)
        # ax1.plot(k_z, min_band, marker="o", color= polyCol.get_facecolor()[0], label="band {}".format(i+1) )
        # ax1.plot(k_z, max_band, marker="o", color= polyCol.get_facecolor()[0])
#         ax1.plot(k_z, max_band, marker="o")
        
    ax1.plot(k_z, k_z, color="red", label="SOL Dispersion")
    ax1.set_xlabel(r'$k_z a/ 2 \pi$')
    ax1.set_ylabel(r'$\omega a/ 2 \pi c $')
    ax1.set_title(r"Projected Band Diagram")

    
    # shrink axis 
    box = ax1.get_position()
    ax1.set_position([box.x0, box.y0,
                 box.width* 0.8, box.height])
    ax1.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    return fig, ax1


def plot_bands_at_fixed_kz(dirpath, k_point_labels=[r"$\Gamma$" , r"M", r"K", r"$\Gamma$"], numbands=7, num_kt_interpolate=9, k_set=np.linspace(0,1,20, endpoint=False)): 
    '''
    For a given value of kz show the band diagram by the transverse wave vector (kx, ky). According to the existing kz values in the directory, the kz value can be changed.
    
    Parameters: TODO
        dirpath: path
            Path to the directory containing the band files. The band files have the form "glass-hexagonal-cyl-bands-k_z-{0:.3f}.freq.dat"
        k_point_labels: list of strings
            Name of the special points of the Brillouin zone over which the k vector is stepped. The default value are the special points of the 2d trigonal lattice Brillouin zone.  
            
            
        numbands: number of bands plotted. The maximum input value is determined by the number of bands simulated in MPB
        num_kz: number of kz over which the simulation was stepped
        num_kt_interpolate: number of interpolated points between characteristic points of the Brilloin zone used in the simulation
    '''
    
    filelist=glob.glob(os.path.join(dirpath, "glass-hexagonal-cyl-bands-k_z-*.freq.dat"))
    
    kz_iter= map(lambda filepath: float(os.path.split(filepath)[1][-14:-9]), filelist)
    kz= np.fromiter(kz_iter, dtype=float)
    # within one of the arrays:
    # k index, k1, k2, k3, kmag/2pi, band 1, band 2, band 3, band 4, band 5, band 6, band 7, band 8
    data= np.array( [ read_bands_freq(filepath) for filepath in filelist ] )
    
    num_kz = len(kz)
    
    fig = plt.figure( figsize= 1.2*plt.figaspect(1.1))
    ax1= fig.add_subplot(1,1,1)
    for band in data[0].T[5:]:
        ax1.plot(data[0, :, 0] -1 , band, marker="o", linestyle="-")
    # print(data[0, :, 0])
    ax1.set_ylabel(r"$\omega a / 2\pi c$")
    ax1.set_xlabel(r"$k a/ 2\pi $")
    ax1.set_xticklabels([])
#     # Gamma X M R  X Gamma Z R T Z Gamma
    # k_points = ["", r"$\Gamma$" , r"M", r"K", r"$\Gamma$"]
    num_kt=data[0].shape[0]
    num_kt_interpolate= (num_kt - len(k_point_labels)) / (len(k_point_labels) -1)
 
    k_point_labels.insert(0,"")
    k_points= k_point_labels
#     # http://stackoverflow.com/questions/30482727/pyplot-setting-grid-line-spacing-for-plot
#     #Spacing between each line
#     #intervals = float(5)
    intervals = float(num_kt_interpolate+1)
    loc = plticker.MultipleLocator(base=intervals)
#     # matplotlib.ticker.IndexLocator(base, offset)  would be better for index plotting
    ax1.xaxis.set_major_locator(loc)
    ax1.xaxis.set_minor_locator(plticker.MultipleLocator(1))
    ax1.grid(which='major', axis='both', linestyle='-')
    ax1.set_xticklabels(k_points)
    ax1.set_title(r"$k_z = {0:.2f} $".format(data[0,0,3]) )
    
    
    
    fix_ylim = True
    def update_by_kz(index_kz=0):
        for line, band in zip(ax1.lines, data[index_kz].T[5:]):
            line.set_ydata(band)
            ax1.set_title(r"$k_z a / 2 \pi = {0:.2f} $".format(data[index_kz,0,3]) )
        update_lower_y_lim(fix_ylim)
#         fig.canvas.draw()
    
    def update_lower_y_lim(bool_ylim=True):
        global fix_ylim
        fix_ylim = bool_ylim
        ax1.relim()
        ax1.autoscale_view()
        if bool_ylim:
            ax1.set_ylim(0, None, auto=True)
        
    
    interact(update_by_kz, index_kz=widgets.IntSlider(min=0,max=num_kz-1,step=1,value=0))
    interact(update_lower_y_lim, bool_ylim=widgets.Checkbox(
        value=True,
        description='Fix lower y limit at 0'
    ))
    

def filter_out(arr, without_indizes_arr):
    '''
    helper function to replace "isin" from numpy
    '''
    def isin_arr(f, arr):
        return (f == arr).any()
    def mask_is_in(arr, filter_arr):
        temp_not_in_arr= lambda f: not isin_arr(f, filter_arr)
        return map(temp_not_in_arr, arr)
    mask= np.fromiter(mask_is_in(np.arange(len(arr)), without_indizes_arr ), dtype=bool)
    return arr[mask]

