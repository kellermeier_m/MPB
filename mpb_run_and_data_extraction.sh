# Run mpb and output the data into .out file
mpb bandstructure.ctl >& bandstructure.out
# Extract the frequencies for TM modes from the output
grep tmfreq bandstructure.out > bandstructure.tm-freq.dat
# Extract the frequencies for TE modes from the output
grep tefreq bandstructure.out > bandstructure.te-freq.dat
 