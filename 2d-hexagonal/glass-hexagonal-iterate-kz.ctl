(set! num-bands 8)

(define-param RADIUS 0.2)
(define-param EPSILON (* 1.95 1.95))
(define-param K_Z_MIN 0.0)  ; Out-of-plane component
(define-param K_Z_MAX 1.0)  ; Out-of-plane component
(define-param K_Z_NUM 50)

(define lattice-constant 1 )  ; reference scale, a

(define Glass (make dielectric (epsilon EPSILON)))

;(set! geometry-lattice (make lattice (size lattice-constant lattice-constant lattice-constant)
          ; The third component of the lattice size is actually not relevant for
          ; defining the crystal since it's continuous in this dir. The size is
          ; only used for reference of the wavevector size
(set! geometry-lattice (make lattice (size lattice-constant lattice-constant no-size)
                         (basis1 (/ (sqrt 3) 2) 0.5)
                         (basis2 (/ (sqrt 3) 2) -0.5)))

(set! geometry (list (make cylinder
                       (center 0 0 0)
                       (radius RADIUS)
                       (height infinity)
                       (material Glass)
                       )))
(define k_z (/ K_Z lattice-constant)) ; used have the input K_Z  in units of 1/a
(set-param! resolution 64)

(define (run-different-kz current_kz kmax dkz)
  (if (<= current_kz kmax)
    (begin
      ; TODO
      (set! k-points (list (vector3 0 0 current_kz)          ; Gamma
                           (vector3 0 0.5 current_kz )        ; M
                           (vector3 (/ -3) (/ 3) current_kz) ; K
                           (vector3 0 0 current_kz)))        ; Gamma
      (set! k-points (interpolate 9 k-points))
      (run display-zparities display-yparities)

      (run-different-kz (+ current_kz dkz) kmax dkz)
    )
  )
)
(run-different-kz K_Z_MIN K_Z_MAX (/ (- K_Z_MAX K_Z_MIN) (K_Z_NUM +1) )) 

;(run-tm (output-at-kpoint (vector3 (/ -3) (/ 3) 0)
;                          fix-efield-phase output-efield-z))
;
;(run-te)
