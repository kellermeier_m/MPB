; Based on tri-rods.ctl in the Tutorial
; Geometry parameters
; Distance between rods in x resp. y direction
(define-param DISTANCE 1)  ; corresponds to 'a'  in Cowan, B.
(define-param VERTICAL_PERIOD (* DISTANCE (sqrt 2)) ); corresponds to 'c'  in Cowan, B.
; (define-param VERTICAL_PERIOD 1 )
; Rod dimensions
(define-param RODWIDTH 0.333)  ; corresponds to 'w'  in Cowan, B.
(define-param RODHEIGHT (* VERTICAL_PERIOD 0.25 ) ) ; corresponds to 'c'/4  in Cowan, B.,
      ; c= a * sqrt(2) gives a cubic lattice
; Material
(define-param EPSILON (* 1.95 1.95))

(define-param GRID_SIZE_X DISTANCE) ;
(define-param GRID_SIZE_Y DISTANCE) ;
(define-param GRID_SIZE_Z VERTICAL_PERIOD) ; increased size due to added pml layer
; (define-param DPML PAD)  ; thickness of the pml layer

(define Silicon (make dielectric (epsilon 12.1)))
(define Glass (make dielectric (epsilon EPSILON)))
(define geometryList (list  ) )
; ----------------------------------------------------------------------------

(set! num-bands 6)
; (set! geometry-lattice (make lattice (size 1 1 no-size)
                         ; (basis1 (/ (sqrt 3) 2) 0.5)
                         ; (basis2 (/ (sqrt 3) 2) -0.5)))

(set! geometry-lattice (make lattice (size GRID_SIZE_X GRID_SIZE_Y GRID_SIZE_Z)
                            (basis1 1 -1 0)
                            (basis2 1  1 0)
                            (basis3 0 0 1)
                            ; (basis-size DISTANCE DISTANCE VERTICAL_PERIOD)
                        )
)


(set! geometryList (append geometryList
        (list
          (make block
              ;(center (cartesian->lattice (vector3 0 0 (* -1.5 RODHEIGHT ))  ) )
              (center 0 0 (* -1.5 RODHEIGHT )  )
              (size RODWIDTH DISTANCE RODHEIGHT)
              (material Glass)
          )
          ; --------------------------------------NOT FINISHED ---------------------
          
          ;(make block
          ;    (center (cartesian->lattice (vector3 0 0 (* -0.5 RODHEIGHT ))  ) )
          ;    (size DISTANCE RODWIDTH RODHEIGHT)
          ;    (material Glass)
          ;)
          ;;(geometric-object-duplicates (vector3 DISTANCE 0 0) 0  1
          ;  (make block
          ;      (center (cartesian->lattice (vector3 (* -0.5 DISTANCE)  0 (* 0.5 RODHEIGHT ) )  ) )
          ;      (size RODWIDTH DISTANCE RODHEIGHT)
          ;      (material Glass)
          ;  )
          ;  (make block
          ;      (center (cartesian->lattice (vector3 (* 0.5 DISTANCE)  0  (* 0.5 RODHEIGHT ) )  ) )
          ;      (size RODWIDTH DISTANCE RODHEIGHT)
          ;      (material Glass)
          ;  )
          ;;)
          ;;(geometric-object-duplicates (vector3 0 DISTANCE 0) 0  1
          ;  (make block
          ;      (center (cartesian->lattice (vector3 0 (* -0.5 DISTANCE)  (* 1.5 RODHEIGHT ) )  ) )
          ;      (size DISTANCE RODWIDTH RODHEIGHT)
          ;      (material Glass)
          ;  )
          ;  (make block
          ;      (center (cartesian->lattice (vector3 0 (* 0.5 DISTANCE)  (* 1.5 RODHEIGHT ) )  ) )
          ;      (size DISTANCE RODWIDTH RODHEIGHT)
          ;      (material Glass)
          ;  )
          ;)

        )
    )
)

(set! geometry geometryList )


; (set! k-points (list (vector3 0 0 0)          ; Gamma
                     ; (vector3 0 0.5 0)        ; M
                     ; (vector3 (/ -3) (/ 3) 0) ; K
                     ; (vector3 0 0 0)))        ; Gamma

(define Gamma (vector3 0 0 0))
(define X (vector3 0.5 0 0 ) )
(define M (vector3 0.5 0.5 0) ) ; M
(define R (vector3 0.5 0.5 0.5))
(define T (vector3 0 0.5 0.5))
(define Z (vector3 0 0 0.5))

(set! k-points (list Gamma X M R  X Gamma Z R T Z Gamma))

; (print "X point " X "\n")
; (print "X point in real space coord. " (reciprocal->cartesian X)  "\n" )

; (print "Z point " Z "\n")
; (print "Z point in real space coord. " (reciprocal->cartesian Z)  "\n" )

; (print "R point " R "\n")
; (print "R point in real space coord. " (reciprocal->cartesian R)  "\n" )



(set! k-points (interpolate 10 k-points))

(set! resolution 64)

(init-params NO-PARITY false)

(output-epsilon)
(exit)

; (use-output-directory)

;(run (output-at-kpoint R
;			  fix-efield-phase output-efield-z))
; (run-te)
