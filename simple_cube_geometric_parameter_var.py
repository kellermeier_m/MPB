# -*- coding: utf-8 -*-
"""
Created on Mon Apr 10 15:54:31 2017

@author: maxke
"""

import numpy as np
from matplotlib import pyplot as plt
import matplotlib.ticker as plticker
#from subprocess import call
import time
import os

def run_mpb(rod_width=0.25):
    os.chdir("simple-cubic")
#    call(["mpb", "RODWIDTH=0.25", "bandstructure-simple.ctl",
#          "|", "tee", "glass/bands-cuboid-hw-ratio1.41-rod-cell-ratio-0.25.out"])
    cmd_mpb = "mpb RODWIDTH={0:0.3f} bandstructure-simple.ctl | tee glass/bands-cuboid-hw-ratio1.41-rod-cell-ratio-{0:0.3f}.out".format(rod_width)
    print("TO BE EXECUTED:", cmd_mpb)
    os.system(cmd_mpb)
    # After MPB has finished
    # grep freq glass/bands-cuboid-rod-cell-ratio-0.33.out > glass/bands-cuboid-rod-cell-ratio-0.33.freq.dat
    cmd_grep= "grep freq glass/bands-cuboid-hw-ratio1.41-rod-cell-ratio-{0:0.3f}.out > glass/bands-cuboid-hw-ratio1.41-rod-cell-ratio-{0:0.3f}.freq.dat".format(rod_width)
    print("TO BE EXECUTED:", cmd_grep)
    os.system(cmd_grep)
    os.chdir("..")

def plot_band_structure(rod_width=0.25):
    ''' file path is assumed to be correct. Check if you want to run the code
    on your machine
    '''
    path_freq_output= "simple-cubic/glass/bands-cuboid-hw-ratio1.41-rod-cell-ratio-{0:0.3f}.freq.dat".format(rod_width)
    tm_bands_import= np.genfromtxt(path_freq_output, skip_header=1, delimiter=",", dtype="str")
    tm_bands_data = tm_bands_import[:,1:].astype(float)
    k= tm_bands_data[:,0:3]

    fig = plt.figure( figsize= plt.figaspect(0.6))
    ax1= fig.add_subplot(1,1,1)
    for tm_band in tm_bands_data.T[5:] :
        ax1.plot(tm_band, marker="o", linestyle="")

    #ax1.plot(tm_band1, markerstyle="o")

    ax1.set_xticklabels([])
    ax1.set_xlim(left=0)
    ax1.set_ylim(bottom=0)

    # Gamma X M R  X Gamma Z R T Z Gamma
    k_points = ["", r"$\Gamma$" , r"X", r"M", r"R", r"X", r"$\Gamma$", r"Z", r"R", r"T", r"Z", r"$\Gamma$"]

    # http://stackoverflow.com/questions/30482727/pyplot-setting-grid-line-spacing-for-plot
    #Spacing between each line
    #intervals = float(5)
    intervals = float(11)
    loc = plticker.MultipleLocator(base=intervals)
    # matplotlib.ticker.IndexLocator(base, offset)  would be better for index plotting
    ax1.xaxis.set_major_locator(loc)
    ax1.xaxis.set_minor_locator(plticker.MultipleLocator(1))
    ax1.grid(which='major', axis='both', linestyle='-')
    ax1.set_xticklabels(k_points)

    fig.savefig("simple-cubic/glass/glass-bands-cuboid-hw-ratio-1.41-rod-cell-ratio-{0:0.3f}.PNG".format(rod_width))





if __name__ == "__main__":
# Turn interactive plotting off
    plt.ioff()

#    for width in np.linspace(0.2, 0.6, 9):
#        run_mpb(width)
    for width in np.linspace(0.2, 0.6, 9):
        plot_band_structure(width)


    '''

    tm_bands_import= np.genfromtxt("simple-cubic/glass/bands-cuboid-rod-cell-ratio-0.33.freq.dat", skip_header=1, delimiter=",", dtype="str")
    tm_bands_data = tm_bands_import[:,1:].astype(float)
    k= tm_bands_data[:,0:3]

    fig = plt.figure( figsize= plt.figaspect(0.6))
    ax1= fig.add_subplot(1,1,1)
    for tm_band in tm_bands_data.T[5:] :
        ax1.plot(tm_band, marker="o", linestyle="")

    #ax1.plot(tm_band1, markerstyle="o")

    ax1.set_xticklabels([])
    ax1.set_xlim(left=0)
    ax1.set_ylim(bottom=0)

    # Gamma X M R  X Gamma Z R T Z Gamma
    k_points = ["", r"$\Gamma$" , r"X", r"M", r"R", r"X", r"$\Gamma$", r"Z", r"R", r"T", r"Z", r"$\Gamma$"]

    # http://stackoverflow.com/questions/30482727/pyplot-setting-grid-line-spacing-for-plot
    #Spacing between each line
    #intervals = float(5)
    intervals = float(11)
    loc = plticker.MultipleLocator(base=intervals)
    # matplotlib.ticker.IndexLocator(base, offset)  would be better for index plotting
    ax1.xaxis.set_major_locator(loc)
    ax1.xaxis.set_minor_locator(plticker.MultipleLocator(1))
    ax1.grid(which='major', axis='both', linestyle='-')
    ax1.set_xticklabels(k_points)

    fig.savefig("simple-cubic/glass/glass-bands-cuboid-hw-ratio-1.41-rod-cell-ratio-0.33.PNG")
    '''
